var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var cryptoRouter = require("./routes/crypto");
var spotRouter = require("./routes/spot");
var reportsRouter = require("./routes/reports");
const initCronJobs = require("./services/crons");

process.env["TZ"] = "Asia/Kolkata";

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/crypto", cryptoRouter);
app.use("/spot", spotRouter);
app.use("/reports", reportsRouter);

console.log("🚀 ~ process.env.TZ", process.env.TZ, new Date());

// cron job
initCronJobs();

module.exports = app;

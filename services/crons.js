const { LinearClient } = require("bybit-api");
const nodeCron = require("node-cron");
const JSONdb = require("simple-json-db");
const config = require("../config");
const log = require("simple-node-logger").createSimpleLogger("reports.log");

const db = new JSONdb("storage.json");

let API_KEY = config.TESTNET_API_KEY;
let PRIVATE_KEY = config.TESTNET_API_SECRET;

const useLivenet = false;

if (useLivenet) {
    API_KEY = config.LIVE_API_KEY;
    PRIVATE_KEY = config.LIVE_API_SECRET;
}

const debug = (...val) => {
    log.info(...val);
    console.log(...val);
};

const initGetBalanceCron = async () => {
    const client = new LinearClient(
        API_KEY,
        PRIVATE_KEY,

        useLivenet
    );

    const midnightBalance = db.get("midnightBalance");

    const usdtBalance = await client.getWalletBalance("USDT");
    debug("🚀 ~ CRONJOB usdtBalance", usdtBalance);

    const totalBalance = usdtBalance.result["USDT"].equity;
    debug("🚀 ~ CRONJOB calculated totalBalance ", totalBalance);
    debug("🚀 ~ CRONJOB midnightBalance ", midnightBalance);

    if (totalBalance > parseInt(midnightBalance))
        db.set("midnightBalance", totalBalance);
};

const initCronJobs = () => {
    const job = nodeCron.schedule("0 0 */6 * * *", () => initGetBalanceCron());
    console.log("🚀 ~ initCronJobs ~ job", job);

    return job;
};

module.exports = initCronJobs;

const { default: BigNumber } = require("bignumber.js");
const { LinearClient } = require("bybit-api");
const express = require("express");
const config = require("../config");
const router = express.Router();
const log = require("simple-node-logger").createSimpleLogger("crypto.log");

let API_KEY = config.TESTNET_API_KEY;
let PRIVATE_KEY = config.TESTNET_API_SECRET;

const useLivenet = false;

const useAlTTestnet = true;

if (useLivenet) {
    API_KEY = config.LIVE_API_KEY;
    PRIVATE_KEY = config.LIVE_API_SECRET;
}

if (useAlTTestnet) {
    API_KEY = config.TESTNET1_API_KEY;
    PRIVATE_KEY = config.TESTNET1_API_SECRET;
}

const client = new LinearClient(
    API_KEY,
    PRIVATE_KEY,

    useLivenet
);

const debug = (...val) => {
    log.info(...val);
    console.log(...val);
};

// get usdt balance
// get open orders
// get closed orders
// get current positions -> websocket
// get current price -> websocket

router.get("/test", async function (req, res) {
    const response = await client.getSymbols();

    console.log("🚀 ~ async ~ response", response);

    let data = response.result.map((c) => ({
        name: c.name,
        max_leverage: c.leverage_filter.max_leverage,
    }));

    data.sort((a, b) => (a.name > b.name ? 1 : -1));

    data = data.filter((d) => d.max_leverage > 25);

    res.json({ data: data });
});

router.get("/balance", async function (req, res) {
    const response = await client.getWalletBalance({ coin: "USDT" });

    console.log("🚀 ~ async ~ response", response);

    res.json({ data: response });
});

router.get("/orders", async function (req, res) {
    const response = await client.getPosition();

    console.log("🚀 ~ async ~ response", response);

    res.json({ data: response });
});

router.post("/place-order", async function (req, res) {
    debug(
        "Tradingview Alert",
        " at ",
        new Date().toJSON(),
        "Body - " + JSON.stringify(req.body),
        "Query - " + JSON.stringify(req.query)
    );

    const { symbol, action } = req.body;

    const leverage = parseInt(req.query.leverage || 1);
    debug("🚀 ~ leverage", leverage);
    const longTP = parseFloat(req.query.longTP || 1.03);
    debug("🚀 ~ longTP", longTP);
    const longSL = parseFloat(req.query.longSL || 0.97);
    debug("🚀 ~ longSL", longSL);
    const shortTP = parseFloat(req.query.shortTP || 0.97);
    debug("🚀 ~ shortTP", shortTP);
    const shortSL = parseFloat(req.query.shortSL || 1.03);
    debug("🚀 ~ shortSL", shortSL);
    const division = parseInt(req.query.division || 1);
    debug("🚀 ~ division", division);
    const trailing = parseFloat(req.query.trailing || 1.03);
    debug("🚀 ~ trailing", trailing);

    try {
        // Check if position exists
        let isOrderOrPositionExists = false;

        const activeOrderList = await client.getActiveOrderList({
            symbol,
            order_status: "Created,New,PartiallyFilled",
        });
        debug("🚀 ~ activeOrderList", activeOrderList);

        if (
            Array.isArray(activeOrderList.result.data) &&
            activeOrderList.result.data.length > 0
        ) {
            isOrderOrPositionExists = true;
        }

        const activePositions = await client.getPosition({
            symbol,
        });

        debug("🚀 ~ activePositions", activePositions);

        let checkExistingPosition;

        if (
            Array.isArray(activePositions.result) &&
            activePositions.result.length > 0
        ) {
            checkExistingPosition = activePositions.result.find(
                (o) => o.size > 0
            );

            if (checkExistingPosition) isOrderOrPositionExists = true;
        }

        debug("🚀 ~ checkExistingPosition", checkExistingPosition);

        const isLong = action === "long" ? true : false;

        // get current price
        const symbolInfo = await client.getTickers({
            symbol: symbol,
        });

        debug("🚀 ~ symbolInfo", symbolInfo);

        // get market price for qty calculations
        const price = new BigNumber(symbolInfo.result[0].mark_price);
        debug("🚀 ~ price", price);

        // get available balance
        const usdtBalance = await client.getWalletBalance("USDT");
        debug("🚀 ~ usdtBalance", usdtBalance);

        const totalBalance = new BigNumber(usdtBalance.result["USDT"].equity);
        const balance = totalBalance.div(division);
        debug("🚀 ~ calculated balance", balance);

        // set leverage
        const userLeverageResponse = await client.setUserLeverage({
            symbol: symbol,
            buy_leverage: leverage,
            sell_leverage: leverage,
        });

        debug("🚀 ~ userLeverageResponse", userLeverageResponse);

        const qty = balance.times(0.95).div(price).times(leverage);
        debug("🚀 ~ origQty", qty);

        let closedTradeInfo;

        // close the order by placing order in reverse with reduceOnly
        if (isOrderOrPositionExists && checkExistingPosition) {
            const closeTradeRequest = {
                side: isLong ? "Buy" : "Sell",
                symbol: symbol,
                order_type: "Market",
                qty: checkExistingPosition.size,
                close_on_trigger: false,
                reduce_only: true,
                time_in_force: "ImmediateOrCancel",
            };

            debug("🚀 ~ closeTradeRequest", closeTradeRequest);

            // place order
            closedTradeInfo = await client.placeActiveOrder(closeTradeRequest);

            debug("🚀 ~ closedTradeInfo", closedTradeInfo);
        }

        // if position already exists
        // and if it is closed
        // allow or show error - position already exists
        // else allow
        if (isOrderOrPositionExists && closedTradeInfo.result.data) {
            log.info(
                "=============================================================================="
            );
            return res.json({
                status: "cancelled",
                message: "Position already exists",
                data: { activeOrderList, activePositions },
            });
        }

        const tradeRequest = {
            side: isLong ? "Buy" : "Sell",
            symbol: symbol,
            order_type: "Market",
            qty: qty.dp(4).toNumber(),
            close_on_trigger: false,
            reduce_only: false,
            time_in_force: "ImmediateOrCancel",
        };

        debug("🚀 ~ tradeRequest", tradeRequest);

        // place order
        const tradeInfo = await client.placeActiveOrder(tradeRequest);

        debug("🚀 ~ tradeInfo", tradeInfo);

        // Set TakeProfit & StopLoss

        const myPositions = await client.getPosition({
            symbol,
        });

        debug("🚀 ~ myPositions", myPositions);

        const checkPosition = myPositions.result.find((o) => o.size > 0);

        const entryPrice = new BigNumber(checkPosition.entry_price);

        let takeProfit = entryPrice.times(longTP).dp(4).toNumber();
        let stopLoss = entryPrice.times(longSL).dp(4).toNumber();
        let trailingStop = entryPrice
            .times(trailing)
            .minus(entryPrice)
            .dp(4)
            .toNumber();

        if (!isLong) {
            takeProfit = entryPrice.times(shortTP).dp(4).toNumber();
            stopLoss = entryPrice.times(shortSL).dp(4).toNumber();
        }

        debug("🚀 ~ takeProfit", takeProfit);
        debug("🚀 ~ stopLoss", stopLoss);
        debug("🚀 ~ trailingStop", trailingStop);

        const SLRequest = {
            symbol,
            side: isLong ? "Buy" : "Sell",
            stop_loss: stopLoss,
            trailing_stop: trailingStop,
        };

        debug("🚀 ~ SLRequest", SLRequest);

        // set stop loss
        const stopLossOrder = await client.setTradingStop(SLRequest);

        debug("🚀 ~ stopLossOrder", stopLossOrder);

        // Place limit order for takeProfit
        // const orderQty = tradeInfo.result.qty;

        // const limitTradeRequest = {
        //     side: isLong ? "Sell" : "Buy",
        //     symbol: symbol,
        //     order_type: "Limit",
        //     qty: orderQty,
        //     price: takeProfit,
        //     close_on_trigger: true,
        //     reduce_only: true,
        //     time_in_force: "GoodTillCancel",
        // };

        // debug("🚀 ~ limitTradeRequest", limitTradeRequest);

        // const limitTradeInfo = await client.placeActiveOrder(limitTradeRequest);
        // debug("🚀 ~ limitTradeInfo", limitTradeInfo);

        const response = {
            price,
            entryPrice,
            balance,
            qty,
            takeProfit,
            stopLoss,
            trailingStop,
            userLeverageResponse,
            tradeInfo,
            stopLossOrder,
            // limitTradeInfo,
        };

        debug("🚀 ~ response", response);

        log.info(
            "=============================================================================="
        );

        return res.json({
            status: "success",
            message: "Order placed successfull",
            data: response,
        });
    } catch (error) {
        debug("🚀 ~ error", error);
        log.info(
            "=============================================================================="
        );
        return res.json({
            status: "error",
            message: "Error occured",
            data: JSON.stringify(error),
        });
    }
});

module.exports = router;

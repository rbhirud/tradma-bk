const BigNumber = require("bignumber.js");
const { LinearClient } = require("bybit-api");
const express = require("express");
const log = require("simple-node-logger").createSimpleLogger("alerts.log");
const config = require("../config");

const JSONdb = require("simple-json-db");

const db = new JSONdb("storage.json");

const router = express.Router();

let API_KEY = config.TESTNET_API_KEY;
let PRIVATE_KEY = config.TESTNET_API_SECRET;

const useLivenet = false;

if (useLivenet) {
    API_KEY = config.LIVE_API_KEY;
    PRIVATE_KEY = config.LIVE_API_SECRET;
}

const client = new LinearClient(
    API_KEY,
    PRIVATE_KEY,

    useLivenet
);

const debug = (...val) => {
    log.info(...val);
    console.log(...val);
};

const calcPercent = (num, percentage) => {
    const number = new BigNumber(num);

    return number.times(new BigNumber(percentage).div(100)).toNumber();
};

router.post("/place-order", async function (req, res) {
    log.info(
        "Tradingview Alert",
        " at ",
        new Date().toJSON(),
        "Body - " + JSON.stringify(req.body),
        "Query - " + JSON.stringify(req.query)
    );

    debug("🚀 ~ req.query", req.query);

    const { symbol, action } = req.body;

    const direction = db.get("direction");

    if (direction.symbol !== symbol && direction.action !== action) {
        log.info(
            "=============================================================================="
        );
        return res.json({
            status: "cancelled",
            message: "Wrong symbol or direction",
            data: { direction },
        });
    }

    const leverage = parseInt(req.query.leverage || 1);
    const longTP = parseFloat(req.query.longTP || 1.005);
    const longSL = parseFloat(req.query.longSL || 0.996);
    const shortTP = parseFloat(req.query.shortTP || 0.995);
    const shortSL = parseFloat(req.query.shortSL || 1.004);
    const division = parseInt(req.query.division || 5);
    debug("🚀 ~ division", {
        division,
        leverage,
        longTP,
        longSL,
        shortTP,
        shortSL,
    });

    debug("🚀 ~ req.body", req.body);

    try {
        // Check if position exists
        let isOrderOrPositionExists = false;

        const activeOrderList = await client.getActiveOrderList({
            symbol,
            order_status: "Created,New,PartiallyFilled",
        });
        debug("🚀 ~ activeOrderList", activeOrderList);

        if (
            Array.isArray(activeOrderList.result.data) &&
            activeOrderList.result.data.length > 0
        ) {
            isOrderOrPositionExists = true;
        }

        const activePositions = await client.getPosition({
            symbol,
        });

        debug("🚀 ~ activePositions", activePositions);

        if (
            Array.isArray(activePositions.result) &&
            activePositions.result.length > 0
        ) {
            const checkPosition = activePositions.result.find(
                (o) => o.size > 0
            );

            if (checkPosition) isOrderOrPositionExists = true;
        }

        if (isOrderOrPositionExists) {
            log.info(
                "=============================================================================="
            );
            return res.json({
                status: "cancelled",
                message: "Order already exists",
                data: { activeOrderList, activePositions },
            });
        }

        const isLong = action === "long" ? true : false;

        // get current price
        const symbolInfo = await client.getTickers({
            symbol: symbol,
        });

        debug("🚀 ~ symbolInfo", symbolInfo);

        const price = new BigNumber(symbolInfo.result[0].mark_price);
        debug("🚀 ~ price", price);

        // get available balance
        const usdtBalance = await client.getWalletBalance("USDT");
        debug("🚀 ~ usdtBalance", usdtBalance);

        const totalBalance = new BigNumber(usdtBalance.result["USDT"].equity);
        const balance = totalBalance.div(division);
        debug("🚀 ~ calculated balance", balance);

        // set leverage
        const marginSwitchResponse = await client.setMarginSwitch({
            symbol: symbol,
            is_isolated: true,
            buy_leverage: leverage,
            sell_leverage: leverage,
        });

        debug("🚀 ~ marginSwitchResponse", marginSwitchResponse);

        const qty = balance.times(0.95).div(price).times(leverage);
        debug("🚀 ~ origQty", qty);

        const tradeRequest = {
            side: isLong ? "Buy" : "Sell",
            symbol: symbol,
            order_type: "Market",
            qty: qty.dp(4).toNumber(),
            price: price.toNumber(),
            close_on_trigger: false,
            reduce_only: false,
            time_in_force: "ImmediateOrCancel",
        };

        debug("🚀 ~ tradeRequest", tradeRequest);

        // place order
        const tradeInfo = await client.placeActiveOrder(tradeRequest);

        debug("🚀 ~ tradeInfo", tradeInfo);

        // Set TakeProfit & StopLoss

        const myPositions = await client.getPosition({
            symbol,
        });

        debug("🚀 ~ myPositions", myPositions);

        const checkPosition = myPositions.result.find((o) => o.size > 0);

        const markPrice = new BigNumber(checkPosition.entry_price);

        let takeProfit = markPrice.times(longTP).dp(4).toNumber();
        let stopLoss = markPrice.times(longSL).dp(4).toNumber();

        if (!isLong) {
            takeProfit = markPrice.times(shortTP).dp(4).toNumber();
            stopLoss = markPrice.times(shortSL).dp(4).toNumber();
        }

        debug("🚀 ~ takeProfit", takeProfit);
        debug("🚀 ~ stopLoss", stopLoss);

        // set stop loss
        const stopLossOrder = await client.setTradingStop({
            symbol,
            side: isLong ? "Buy" : "Sell",
            stop_loss: stopLoss,
        });

        debug("🚀 ~ stopLossOrder", stopLossOrder);

        // Place limit order for takeProfit
        const orderQty = tradeInfo.result.qty;

        const limitTradeRequest = {
            side: isLong ? "Sell" : "Buy",
            symbol: symbol,
            order_type: "Limit",
            qty: orderQty,
            price: takeProfit,
            close_on_trigger: true,
            reduce_only: true,
            time_in_force: "GoodTillCancel",
        };

        debug("🚀 ~ limitTradeRequest", limitTradeRequest);

        const limitTradeInfo = await client.placeActiveOrder(limitTradeRequest);
        debug("🚀 ~ limitTradeInfo", limitTradeInfo);

        const response = {
            price,
            markPrice,
            balance,
            qty,
            takeProfit,
            stopLoss,
            marginSwitchResponse,
            tradeInfo,
            stopLossOrder,
            limitTradeInfo,
        };

        debug("🚀 ~ response", response);

        log.info(
            "=============================================================================="
        );

        return res.json({
            status: "success",
            message: "Order placed successfull",
            data: response,
        });
    } catch (error) {
        debug("🚀 ~ error", error);
        log.info(
            "=============================================================================="
        );
        return res.json({
            status: "error",
            message: "Error occured",
            data: JSON.stringify(error),
        });
    }
});

router.post("/place-ema-order", async function (req, res) {
    debug(
        "Tradingview Alert",
        " at ",
        new Date().toJSON(),
        "Body - " + JSON.stringify(req.body),
        "Query - " + JSON.stringify(req.query)
    );

    const { symbol, action } = req.body;

    const direction = db.get("direction");
    const midnightBalance = db.get("midnightBalance");

    const leverage = parseInt(req.query.leverage || 1);
    debug("🚀 ~ leverage", leverage);
    const longTP = parseFloat(req.query.longTP || 1.005);
    debug("🚀 ~ longTP", longTP);
    const longSL = parseFloat(req.query.longSL || 0.996);
    debug("🚀 ~ longSL", longSL);
    const shortTP = parseFloat(req.query.shortTP || 0.995);
    debug("🚀 ~ shortTP", shortTP);
    const shortSL = parseFloat(req.query.shortSL || 1.004);
    debug("🚀 ~ shortSL", shortSL);
    const division = parseInt(req.query.division || 5);
    debug("🚀 ~ division", division);
    const percent = parseInt(req.query.percent || 5.27);
    debug("🚀 ~ percent", percent);

    try {
        // Check if position exists
        let isOrderOrPositionExists = false;

        const activeOrderList = await client.getActiveOrderList({
            symbol,
            order_status: "Created,New,PartiallyFilled",
        });
        debug("🚀 ~ activeOrderList", activeOrderList);

        if (
            Array.isArray(activeOrderList.result.data) &&
            activeOrderList.result.data.length > 0
        ) {
            isOrderOrPositionExists = true;
        }

        const activePositions = await client.getPosition({
            symbol,
        });

        debug("🚀 ~ activePositions", activePositions);

        let checkExistingPosition;

        if (
            Array.isArray(activePositions.result) &&
            activePositions.result.length > 0
        ) {
            checkExistingPosition = activePositions.result.find(
                (o) => o.size > 0
            );

            if (checkExistingPosition) isOrderOrPositionExists = true;
        }

        debug("🚀 ~ checkExistingPosition", checkExistingPosition);

        const isLong = action === "long" ? true : false;

        // get current price
        const symbolInfo = await client.getTickers({
            symbol: symbol,
        });

        debug("🚀 ~ symbolInfo", symbolInfo);

        // get market price for qty calculations
        const price = new BigNumber(symbolInfo.result[0].mark_price);
        debug("🚀 ~ price", price);

        // get available balance
        const usdtBalance = await client.getWalletBalance("USDT");
        debug("🚀 ~ usdtBalance", usdtBalance);

        const totalBalance = new BigNumber(usdtBalance.result["USDT"].equity);
        const balance = totalBalance.div(division);
        debug("🚀 ~ calculated balance", balance);

        // set leverage
        const userLeverageResponse = await client.setUserLeverage({
            symbol: symbol,
            buy_leverage: leverage,
            sell_leverage: leverage,
        });

        debug("🚀 ~ userLeverageResponse", userLeverageResponse);

        const qty = balance.times(0.95).div(price).times(leverage);
        debug("🚀 ~ origQty", qty);

        let closedTradeInfo;

        // close the order by placing order in reverse with reduceOnly
        if (isOrderOrPositionExists && checkExistingPosition) {
            const closeTradeRequest = {
                side: isLong ? "Buy" : "Sell",
                symbol: symbol,
                order_type: "Market",
                qty: checkExistingPosition.size,
                close_on_trigger: false,
                reduce_only: true,
                time_in_force: "ImmediateOrCancel",
            };

            debug("🚀 ~ closeTradeRequest", closeTradeRequest);

            // place order
            closedTradeInfo = await client.placeActiveOrder(closeTradeRequest);

            debug("🚀 ~ closedTradeInfo", closedTradeInfo);
        }

        // if position already exists
        // and if it is closed
        // allow or show error - position already exists
        // else allow
        if (isOrderOrPositionExists && closedTradeInfo.result.data) {
            log.info(
                "=============================================================================="
            );
            return res.json({
                status: "cancelled",
                message: "Position already exists",
                data: { activeOrderList, activePositions },
            });
        }

        const midnightThreshhold = new BigNumber(
            calcPercent(midnightBalance, percent)
        )
            .plus(midnightBalance)
            .toNumber();

        debug(
            "🚀 ~ midnightThreshhold",
            midnightBalance,
            percent,
            midnightThreshhold
        );

        if (balance.gt(midnightThreshhold)) {
            log.info(
                "=============================================================================="
            );
            return res.json({
                status: "cancelled",
                message: "Daily profit reached",
                midnightBalance,
                balance,
                percent: percent + "%",
                midnightThreshhold,
            });
        }

        debug("🚀 ~ direction", direction);

        if (direction.symbol !== symbol || direction.action !== action) {
            log.info(
                "=============================================================================="
            );
            return res.json({
                status: "cancelled",
                message: "Wrong symbol or direction",
                data: { direction },
            });
        }

        const tradeRequest = {
            side: isLong ? "Buy" : "Sell",
            symbol: symbol,
            order_type: "Market",
            qty: qty.dp(4).toNumber(),
            close_on_trigger: false,
            reduce_only: false,
            time_in_force: "ImmediateOrCancel",
        };

        debug("🚀 ~ tradeRequest", tradeRequest);

        // place order
        const tradeInfo = await client.placeActiveOrder(tradeRequest);

        debug("🚀 ~ tradeInfo", tradeInfo);

        // Set TakeProfit & StopLoss

        const myPositions = await client.getPosition({
            symbol,
        });

        debug("🚀 ~ myPositions", myPositions);

        const checkPosition = myPositions.result.find((o) => o.size > 0);

        const markPrice = new BigNumber(checkPosition.entry_price);

        let takeProfit = markPrice.times(longTP).dp(4).toNumber();
        let stopLoss = markPrice.times(longSL).dp(4).toNumber();

        if (!isLong) {
            takeProfit = markPrice.times(shortTP).dp(4).toNumber();
            stopLoss = markPrice.times(shortSL).dp(4).toNumber();
        }

        debug("🚀 ~ takeProfit", takeProfit);
        debug("🚀 ~ stopLoss", stopLoss);

        const SLRequest = {
            symbol,
            side: isLong ? "Buy" : "Sell",
            stop_loss: stopLoss,
        };

        // set stop loss
        const stopLossOrder = await client.setTradingStop(SLRequest);

        debug("🚀 ~ stopLossOrder", stopLossOrder);

        // Place limit order for takeProfit
        const orderQty = tradeInfo.result.qty;

        const limitTradeRequest = {
            side: isLong ? "Sell" : "Buy",
            symbol: symbol,
            order_type: "Limit",
            qty: orderQty,
            price: takeProfit,
            close_on_trigger: true,
            reduce_only: true,
            time_in_force: "GoodTillCancel",
        };

        debug("🚀 ~ limitTradeRequest", limitTradeRequest);

        const limitTradeInfo = await client.placeActiveOrder(limitTradeRequest);
        debug("🚀 ~ limitTradeInfo", limitTradeInfo);

        const response = {
            price,
            markPrice,
            balance,
            qty,
            takeProfit,
            stopLoss,
            userLeverageResponse,
            tradeInfo,
            stopLossOrder,
            limitTradeInfo,
        };

        debug("🚀 ~ response", response);

        log.info(
            "=============================================================================="
        );

        return res.json({
            status: "success",
            message: "Order placed successfull",
            data: response,
        });
    } catch (error) {
        debug("🚀 ~ error", error);
        log.info(
            "=============================================================================="
        );
        return res.json({
            status: "error",
            message: "Error occured",
            data: JSON.stringify(error),
        });
    }
});

router.post("/place-tpsl", async function (req, res) {
    log.info(
        "Tradingview Alert",
        " at ",
        new Date().toJSON(),
        "Body - " + JSON.stringify(req.body),
        "Query - " + JSON.stringify(req.query)
    );

    debug("🚀 ~ req.query", req.query);

    const { symbol, action } = req.body;

    const longTP = parseFloat(req.query.longTP || 1.005);
    debug("🚀 ~ longTP", longTP);
    const longSL = parseFloat(req.query.longSL || 0.996);
    debug("🚀 ~ longSL", longSL);
    const shortTP = parseFloat(req.query.shortTP || 0.995);
    debug("🚀 ~ shortTP", shortTP);
    const shortSL = parseFloat(req.query.shortSL || 1.004);
    debug("🚀 ~ shortSL", shortSL);

    debug("🚀 ~ req.body, name, symbol, type, action", req.body);

    try {
        const isLong = action === "long" ? true : false;

        // Set TakeProfit & StopLoss
        const myPositions = await client.getPosition({
            symbol,
        });

        debug("🚀 ~ myPositions", myPositions);

        const checkPosition = myPositions.result.find((o) => o.size > 0);

        const markPrice = new BigNumber(checkPosition.entry_price);

        let takeProfit = markPrice.times(longTP).dp(4).toNumber();
        let stopLoss = markPrice.times(longSL).dp(4).toNumber();

        if (!isLong) {
            takeProfit = markPrice.times(shortTP).dp(4).toNumber();
            stopLoss = markPrice.times(shortSL).dp(4).toNumber();
        }

        debug("🚀 ~ takeProfit", takeProfit);
        debug("🚀 ~ stopLoss", stopLoss);

        // set stop loss
        const stopLossOrder = await client.setTradingStop({
            symbol,
            side: isLong ? "Buy" : "Sell",
            stop_loss: stopLoss,
        });

        debug("🚀 ~ stopLossOrder", stopLossOrder);

        // Place limit order for takeProfit
        const orderQty = checkPosition.qty;

        const limitTradeRequest = {
            side: isLong ? "Sell" : "Buy",
            symbol: symbol,
            order_type: "Limit",
            qty: orderQty,
            price: takeProfit,
            close_on_trigger: true,
            reduce_only: true,
            time_in_force: "GoodTillCancel",
        };

        debug("🚀 ~ limitTradeRequest", limitTradeRequest);

        const limitTradeInfo = await client.placeActiveOrder(limitTradeRequest);
        debug("🚀 ~ limitTradeInfo", limitTradeInfo);

        const response = {
            markPrice,
            takeProfit,
            stopLoss,
            stopLossOrder,
            limitTradeInfo,
        };

        debug("🚀 ~ response", response);

        log.info(
            "=============================================================================="
        );

        return res.json({
            status: "success",
            message: "Order placed successfull",
            data: response,
        });
    } catch (error) {
        debug("🚀 ~ error", error);
        log.info(
            "=============================================================================="
        );
        return res.json({
            status: "error",
            message: "Error occured",
            data: JSON.stringify(error),
        });
    }
});

module.exports = router;

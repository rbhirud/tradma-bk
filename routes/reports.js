const { LinearClient } = require("bybit-api");
const express = require("express");
const config = require("../config");
const router = express.Router();
const moment = require("moment");
const log = require("simple-node-logger").createSimpleLogger("reports.log");
const BigNumber = require("bignumber.js");
const JSONdb = require("simple-json-db");

const db = new JSONdb("storage.json");

let API_KEY = config.TESTNET_API_KEY;
let PRIVATE_KEY = config.TESTNET_API_SECRET;

const useLivenet = false;

if (useLivenet) {
    API_KEY = config.LIVE_API_KEY;
    PRIVATE_KEY = config.LIVE_API_SECRET;
}

const client = new LinearClient(
    API_KEY,
    PRIVATE_KEY,

    useLivenet
);

const debug = (...val) => {
    log.info(...val);
    console.log(...val);
};

router.post("/set-big-direction", async function (req, res) {
    const { symbol, action } = req.body;

    const bigDirection = {
        symbol,
        action,
    };

    debug("🚀 ~ bigDirection", bigDirection);

    db.set("direction", bigDirection);

    return res.json({
        status: "success",
        message: "Success",
        data: bigDirection,
    });
});

router.get("/daily-pnr", async function (req, res) {
    log.info(
        "Tradingview Alert",
        " at ",
        new Date().toJSON(),
        "Body - " + JSON.stringify(req.body),
        "Query - " + JSON.stringify(req.query)
    );

    debug("🚀 ~ req.query", req.query);

    const { symbol } = req.query;

    try {
        const dailyPnL = await getDailyPNLPerc(client, symbol);

        db.set("startingBalance", dailyPnL);

        const response = {
            dailyPnL,
        };

        debug("🚀 ~ response", response);

        log.info(
            "=============================================================================="
        );

        return res.json({
            status: "success",
            message: "Success",
            data: response,
        });
    } catch (error) {
        debug("🚀 ~ error", error);
        return res.json({
            status: "error",
            message: "Error occured",
            data: JSON.stringify(error),
        });
    }
});

// get all pnl list
const getDailyPNLLISt = async (client, symbol) => {
    const req = {
        symbol,
        start_time: moment().startOf("day").format("X"),
        end_time: moment().endOf("day").format("X"),
    };

    debug("🚀 ~ getDailyPNLLISt ~ req", req);

    const getClosedPnl = await client.getClosedPnl(req);

    debug("🚀 ~ getDailyPNLLISt ~ getClosedPnl", getClosedPnl);

    return getClosedPnl.result.data;
};

// calculate daily profit in percent
const getDailyPNLPerc = async (client, symbol) => {
    const pnlList = await getDailyPNLLISt(client, symbol);

    const pnlArr = pnlList.map((p) => p.closed_pnl);
    debug("🚀 ~ getDailyPNLPerc ~ pnlArr", pnlArr);

    const sum = pnlArr.reverse().reduce((sum, a) => {
        const sumBN = new BigNumber(sum);

        debug(
            "🚀 ~ sum ~ sumBN",
            sumBN.toNumber(),
            a,
            sumBN.plus(a).toNumber()
        );

        return sumBN.plus(a);
    }, 0);

    return sum;
};

module.exports = router;

\*\*\* Spot Trading (1x Leverage)

1. Buy at crossover (Long or Short)
2. Use trailing stop
3. Sell after trailing stop

-   Conditions

1. 5-10% per day profit
2. Trade at 5 coins initially

-   STRATEGY 1

1. 2% TP & 1% SL
2. On Engulfing candle close
3. 1x Leverage

---

\*\*\* Leverage Trading (50x Leverage)

1. Buy at 3BigAss signal
2. Add TP at 0.5 & SL at 0.4

-   Conditions

1. Monitor results for 1 month
2. Improve the strategy
